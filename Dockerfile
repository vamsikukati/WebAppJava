FROM tomcat

ADD target/StrutsLoginEx.war /usr/local/tomcat/webapps/

CMD ["catalina.sh", "run"]